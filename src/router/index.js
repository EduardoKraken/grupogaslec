import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Contacto from '../views/Contactanos.vue'

Vue.use(VueRouter)

const routes = [
  { path: '/',         name: 'Home',     component: Home },
  { path: '/contacto', name: 'Contacto', component: Contacto },
]

const router = new VueRouter({
  routes
})

export default router
